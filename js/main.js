$(document).ready(function() {
	
	// **************************** For scroll menu
	$(window).scroll(function(){

		if($(this).scrollTop()>190){
			$('#menu').addClass('fixed');
			$('.header-logo-img').addClass('small');
			$('.header-menu-list').addClass('small-list');
			$('.header-menu-links').addClass('small-links');
		}
		else if ($(this).scrollTop()<190){
			$('#menu').removeClass('fixed');
			$('.header-logo-img').removeClass('small');
			$('.header-menu-list').removeClass('small-list');
			$('.header-menu-links').removeClass('small-links');
		}
	});

	// **************************** For form validate
	$(function(){
		$('#contacts').validate();

		$('#name').rules('add', {
			required: true,
			minlength: 2,
			messages:{
				required: "the value 'Your name' must not be empty"
			}
		});

		$('#email').rules('add', {
			required: true,
			email: true,
			messages:{
				required: "the value 'Your email' must not be empty"
			}
		});

		$('#text').rules('add', {
			required: true,
			minlength: 2,
			messages:{
				required: "the value 'Your message' must not be empty"
			}
		});
	});

	// **************************** For burger menu
	$(function(){
		$('.burger-menu').on('click', function(){
			$(this).closest('.header-menu-nav').toggleClass('menu-open');
		});
	});
});